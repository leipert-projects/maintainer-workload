import path from "path";
import replace from "@rollup/plugin-replace";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import { terser } from "rollup-plugin-terser";
import vue from "rollup-plugin-vue";
import copy from "rollup-plugin-copy";
import postcss from "rollup-plugin-postcss";
import analyze from "rollup-plugin-analyzer";

const COMMIT = process.env.CI_COMMIT_SHORT_SHA || "00000000";

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

import { version as svgVersion } from "@gitlab/svgs/package.json";
const SVG_FILE = `icons-${svgVersion}.svg`;
import { version as vueVersion } from "vue/package.json";

const VUE_BUILD = production ? "vue.runtime.min.js" : "vue.runtime.js";
const VUE_FILE = VUE_BUILD.replace("vue.", `vue.${vueVersion}.`);

function GitLabSVGLoader() {
  return {
    name: "gitlab-svg-loader",
    load: function load(id) {
      if (!id.includes("@gitlab/svgs/dist")) {
        return null;
      }

      console.log(`Replace import for ${id} with './${SVG_FILE}'`);

      return `export default './${SVG_FILE}'`;
    },
  };
}

function PopperLoader() {
  return {
    name: "popper-loader",
    load: function load(id) {
      if (!id.includes("/popper.js/")) {
        return null;
      }

      console.log(`Replace import for ${id} with 'undefined'`);

      return "export default undefined;";
    },
  };
}

function TooltipLoader() {
  return {
    name: "tooltip-loader",
    load: function load(id) {
      if (!id.includes("tooltip/tooltip.vue")) {
        return null;
      }

      console.log(`Replace import for ${id} with empty component'`);

      return "export default {render:() => null};";
    },
  };
}

const rootDir = path.resolve(__dirname, "..");
const sourceDir = path.resolve(__dirname);
const targetDir = path.resolve(rootDir, "public");

export default () => ({
  input: path.join(sourceDir, "main.js"),
  external: ["vue"],
  output: {
    file: path.join(targetDir, `bundle-${COMMIT}.js`),
    format: "iife", // immediately-invoked function expression — suitable for <script> tags
    sourcemap: true,
    globals: {
      vue: "Vue",
    },
  },
  plugins: [
    copy({
      targets: [
        {
          src: path.join(rootDir, "node_modules/@gitlab/svgs/dist/icons.svg"),
          dest: targetDir,
          rename: SVG_FILE,
        },
        {
          src: path.join(rootDir, `node_modules/vue/dist/${VUE_BUILD}`),
          dest: targetDir,
          rename: VUE_FILE,
        },
        {
          src: path.join(rootDir, `webapp/index.html`),
          dest: targetDir,
          transform: (contents) =>
            contents
              .toString()
              .replace("__VUE_BUILD__", `./${VUE_FILE}`)
              .replace(/__COMMIT__/g, COMMIT),
        },
      ],
    }),
    replace({
      "process.env.NODE_ENV": '"production"',
    }),
    GitLabSVGLoader(),
    PopperLoader(),
    TooltipLoader(),
    resolve(), // Resolve dependencies
    commonjs(), // convert them to commonjs
    postcss({
      extract: true,
    }),
    vue({
      needMap: false,
    }),
    production && terser(), // minify, but only in production
    production && analyze(),
  ],
});
