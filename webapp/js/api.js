export const getStats = async () => {
  const response = await fetch("./stats.json");

  return response.json();
};
