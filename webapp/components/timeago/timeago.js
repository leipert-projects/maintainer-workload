import en_US from "timeago.js/esm/lang/en_US";
import { register } from "timeago.js/esm/register";
import { render } from "timeago.js/esm/realtime";
register("en_US", en_US);

export default render;
