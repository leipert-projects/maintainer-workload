#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
const { PurgeCSS } = require("purgecss");
const postcss = require("postcss");
const autoprefixer = require("autoprefixer");
const csso = require("postcss-csso");

const minifier = postcss([csso, autoprefixer]);

const publicDir = path.join(__dirname, "..", "..", "public");

const minify = async ({ css, file }) => {
  return {
    file,
    css: (await minifier.process(css, { from: file })).css,
  };
};

const main = async () => {
  const purged = await new PurgeCSS().purge({
    content: [
      path.join(publicDir, "bundle-*.js"),
      path.join(publicDir, "index.html"),
    ],
    css: [path.join(publicDir, "bundle-*.css")],
    whitelistPatternsChildren: [
      /gl-button/,
      /gl-avatar/,
      /gl-alert/,
      /gl-icon/,
    ],
    keyframes: true,
    variables: true,
  });

  const minified = await Promise.all(purged.map(minify));

  minified.forEach(({ file, css }) => {
    fs.writeFileSync(file, css);
    console.log(`Minified ${file}`);
  });
};

main().catch((e) => {
  console.warn(e);
  process.exit(1);
});
