import "@gitlab/ui/dist/index.css";
import "@gitlab/ui/dist/utility_classes.css";
import "./main.css";
import Vue from "vue";
import Main from "./main.vue";
import { getStats } from "./js/api.js";

window.addEventListener("DOMContentLoaded", () => {
  new Vue({
    el: "#app",
    render(createElement) {
      return createElement(Main, {
        props: { statsPromise: getStats() },
      });
    },
  });
});
