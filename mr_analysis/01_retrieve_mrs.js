#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
const moment = require("moment");

const { GitLabAPIIterator } = require("./lib/api");
const { loadPrevMr, savePrevMr, saveDatabase } = require("./lib/db_utils");
const { cleanMR, getLastUpdateKey, camelcaseObject } = require("./lib/utils");

const updatedAfter = (slug) => {
  const lastUpdates = JSON.parse(
    fs.readFileSync(path.join(__dirname, "mr_db", "last_update"), "utf8")
  );
  const key = getLastUpdateKey(slug);
  if (lastUpdates[key]) {
    return moment(lastUpdates[key]).toISOString();
  }
  console.log(`${slug} is not initialized, getting last 30 days of data`);

  return moment().subtract(31, "days").toISOString();
};

class MergeRequestIterator extends GitLabAPIIterator {
  constructor(slug, updatedAfter) {
    super(`${slug}/merge_requests`, {
      per_page: 100,
      order_by: "updated_at",
      sort: "asc",
      updated_after: updatedAfter,
    });
  }
}

class NoteIterator extends GitLabAPIIterator {
  constructor(projectID, iid, per_page = 20) {
    super(`/projects/${projectID}/merge_requests/${iid}/notes`, {
      per_page,
      sort: "desc",
      order_by: "updated_at",
    });
  }
}

const getMRNotes = async (mrData, prevNotes = []) => {
  console.log(`${mrData.webUrl} | ${mrData.updatedAt}`);

  const notes = prevNotes;

  const allNotes = new NoteIterator(
    mrData.projectId,
    mrData.iid,
    notes.length ? 20 : 100
  );

  for await (const note of allNotes) {
    if (notes.findIndex((x) => x.id === note.id) >= 0) {
      console.log(`\tFound ${note.id} – break`);
      break;
    }
    note.createdAt = note.created_at;
    notes.push(note);
  }

  console.log(`\tNotes: ${notes.length} notes`);

  return notes;
};

const getData = async (api, slug) => {
  console.log(`Getting data for: ${slug}`);
  const allMRs = new MergeRequestIterator(
    `/${api}/${encodeURIComponent(slug)}`,
    updatedAfter(slug)
  );

  const start = new Date();

  let count = 0;

  for await (const mrData of allMRs) {
    const mr = camelcaseObject(mrData);
    if (new Date() - start >= 5 * 60 * 1000) {
      console.log(
        `getData ran for more than 5 minutes, got ${count} MRs. Stopping iteration.`
      );
      break;
    }

    if (
      mr &&
      mr.author &&
      mr.author.username === "gitlab-bot" &&
      mr.webUrl &&
      mr.webUrl.includes("security-products/tests/")
    ) {
      console.log(`Skipping because noise ${mr.webUrl}`);

      continue;
    }

    const prevMR = await loadPrevMr(mr.id);

    if (prevMR.skipUpdates) {
      console.log(`${prevMR.webUrl}: We have all data - skipping updates`);
      continue;
    }

    const notes = await getMRNotes(mr, prevMR.notes);

    await savePrevMr(cleanMR({ ...prevMR, ...mr, notes }));

    count += 1;
  }
};

const main = async () => {
  await getData("groups", "gitlab-org");
  await getData("projects", "gitlab-com/www-gitlab-com");
  await saveDatabase();
};

main()
  .then(() => {
    console.log("Successfully retrieved data, saving database");
  })
  .catch((e) => {
    console.log("Unhandled error");
    console.log(e);
    process.exit(1);
  });
