const _ = require("lodash");

const matchRegex = /^(re)?assigned to/i;

const parseAssignment = (string) => {
  if (!matchRegex.test(string)) {
    return [];
  }

  const people = [];

  for (let token of string.split(/,?\s/)) {
    if (token === "unassigned") {
      break;
    } else if (token.startsWith("@")) {
      people.push(token);
    }
  }

  return people.sort();
};

const cleanUser = ({ id, username }) => ({ id, username });

const cleanNote = ({ id, body, author, system, createdAt }) => {
  const ret = { id, createdAt, system };

  if (system && matchRegex.test(body)) {
    return { ...ret, body, author: cleanUser(author) };
  }

  return ret;
};

const cleanMR = ({
  id,
  iid,
  projectId,
  state: s,
  createdAt,
  updatedAt,
  mergedAt,
  closedAt,
  author,
  assignees,
  labels,
  references,
  notes,
  webUrl,
}) => {
  const notesMapped = notes.map(cleanNote);

  const state = s.toUpperCase();

  return {
    id,
    iid,
    skipUpdates: notesMapped.length && ["CLOSED", "MERGED"].includes(state),
    projectId,
    state,
    createdAt,
    updatedAt,
    mergedAt,
    closedAt,
    labels,
    references,
    webUrl,
    author: cleanUser(author),
    assignees: assignees.map(cleanUser),
    notes: notesMapped,
  };
};

const getLastUpdateKey = (slug) => {
  const segments = slug.replace(/!.+$/, "").split("/");

  if (segments[0] === "gitlab-org") {
    return segments[0];
  }
  return segments.join("/");
};

const camelcaseObject = (object) => {
  if (_.isPlainObject(object)) {
    const entries = Object.entries(object).map(([key, value]) => {
      return [_.camelCase(key), camelcaseObject(value)];
    });
    return Object.fromEntries(entries);
  }

  if (_.isArray(object)) {
    return object.map(camelcaseObject);
  }

  return object;
};

module.exports = {
  camelcaseObject,
  matchRegex,
  parseAssignment,
  cleanMR,
  getLastUpdateKey,
};
