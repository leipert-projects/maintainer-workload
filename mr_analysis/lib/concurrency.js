/** Concurrency code taken from: https://programming.vip/docs/control-the-concurrency-of-promise.all-through-iterator.html */

// generate workers according to concurrency number
// each worker takes the same iterator
const limit = (concurrency) => (iterator) => {
  const workers = new Array(concurrency);
  return workers.fill(iterator);
};

// run tasks in an iterator one by one
const run = (func) => async (iterator) => {
  for (const item of iterator) {
    await func(item);
  }
};

// wrap limit and run together
function asyncTasks(array, func, concurrency = 1) {
  return limit(concurrency)(array.values()).map(run(func));
}

module.exports = asyncTasks;
