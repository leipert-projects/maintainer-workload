module.exports = function getAverages(stats) {
  const averages = {};

  stats.forEach((x) => {
    const { user, stats, status, mrs } = x;
    const types = (user && user.type) || [];

    for (const type of types) {
      if (!averages[type]) {
        averages[type] = {
          key: `${type}-average`,
          personPower: 0,
          available: 0,
          user: {
            username: null,
            name: `average ${type}`.split("::").reverse().join(" "),
            avatarUrl: null,
            webUrl: null,
            type: [type, "average"],
            tzOffset: 0,
          },
          mrs: [],
          stats: {
            count7: 0,
            count30: 0,
            assignEvents: [],
          },
          status: null,
        };
      }
      averages[type].personPower += 1;
      averages[type].user.tzOffset += user.tzOffset;
      averages[type].mrs = [...averages[type].mrs, ...mrs.map((x) => x.webUrl)];
      averages[type].available += status.available ? 1 : 0;
      averages[type].stats.count7 += stats.count7;
      averages[type].stats.count30 += stats.count30;
      stats.assignEvents.forEach((entry, index) => {
        if (!averages[type].stats.assignEvents[index]) {
          averages[type].stats.assignEvents[index] = {
            value: 0,
            date: entry.date,
          };
        }
        averages[type].stats.assignEvents[index].value += entry.value;
      });
    }
  });

  return Object.values(averages).map((a) => {
    a.status = {
      available: a.available > 0,
      emoji: "",
      messageHtml: `${a.available} of ${
        a.personPower
      } folks available (${parseFloat(
        ((a.available * 100) / a.personPower).toFixed(1)
      )}%).`,
    };
    a.user.tzOffset = a.user.tzOffset / a.personPower;
    a.user.personPower = a.personPower;
    a.stats.avg7 = parseFloat((a.stats.count7 / a.personPower / 7).toFixed(1));
    a.stats.avg30 = parseFloat(
      (a.stats.count30 / a.personPower / 30).toFixed(1)
    );

    a.mrs = {
      average: parseFloat(new Set(a.mrs).size / a.personPower).toFixed(1),
    };
    delete a.personPower;
    delete a.available;
    return a;
  });
};
