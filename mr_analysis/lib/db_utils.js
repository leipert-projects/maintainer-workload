const fs = require("fs");
const { Duplex } = require("stream");
const path = require("path");
const { load, Reader } = require("protobufjs");

const DB_FILE = path.join(__dirname, "../mr_db/db.proto");

let db = null;

let schema = null;

const loadSchema = async () => {
  if (schema) {
    return schema;
  }
  return new Promise((resolve, reject) => {
    load(path.join(__dirname, "../schema.proto"), function (err, root) {
      if (err) {
        return reject(err);
      }

      schema = root;

      return resolve(root);
    });
  });
};

const loadDB = async () => {
  if (db) {
    return db;
  }

  db = {};

  try {
    const schema = await loadSchema();
    const MergeRequest = schema.lookupType("MergeRequest");

    const reader = Reader.create(fs.readFileSync(DB_FILE));
    while (reader.pos < reader.len) {
      const mr = MergeRequest.decodeDelimited(reader);
      db[`${mr.id}`] = mr;
    }

    console.warn(`Loaded ${Object.keys(db).length} MRs`);

    return db;
  } catch (e) {
    console.warn(`Could not load DB_FILE: ${DB_FILE}`);
    return db;
  }
};

const loadPrevMr = async (id) => {
  await loadDB();
  return db[`${id}`] || {};
};

const savePrevMr = async (mr) => {
  await loadDB();
  db[`${mr.id}`] = mr;
};

const getDB = async () => {
  await loadDB();
  return db;
};

const saveDatabase = async (array, fn = (x) => x) => {
  const schema = await loadSchema();
  const MergeRequest = schema.lookupType("MergeRequest");

  return new Promise((resolve) => {
    const fileStream = fs.createWriteStream(path.join(DB_FILE));
    const stream = new Duplex();
    stream.pipe(fileStream);

    fileStream.on("finish", () => {
      resolve();
    });

    const mrs = Array.isArray(array) ? array : Object.values(db);

    for (let i = 0; i < mrs.length; i += 1) {
      const message = MergeRequest.fromObject(fn(mrs[i], i));
      stream.push(MergeRequest.encodeDelimited(message).finish());
    }

    stream.push(null);
  });
};

module.exports = {
  loadPrevMr,
  savePrevMr,
  saveDatabase,
  getDB,
};
