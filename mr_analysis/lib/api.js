const {
  GitLabAPI: gitlabAPI,
  GitLabAPIIterator,
} = require("gitlab-api-async-iterator")();

const memoizedEmoji = {};

const compileEmoji = async (emoji) => {
  if (!memoizedEmoji[emoji]) {
    memoizedEmoji[emoji] = (
      await gitlabAPI.post(`/markdown`, { text: `:${emoji}:`, gfm: true })
    ).data.html.replace(/.+<gl-emoji.+?>(.+?)<\/gl-emoji>.+/, "$1");
  }

  return memoizedEmoji[emoji];
};

module.exports = {
  gitlabAPI,
  GitLabAPIIterator,
  compileEmoji,
};
