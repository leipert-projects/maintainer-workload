#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
const moment = require("moment");

const getAverages = require("./lib/average");
const { GitLabAPIIterator, gitlabAPI } = require("./lib/api");

const getNextBuildFinished = async () => {
  const { data: schedules } = await gitlabAPI.get(
    "/projects/19455943/pipeline_schedules"
  );

  const jobs = new GitLabAPIIterator("/projects/19455943/jobs");
  let count = 0;
  let sum = 0;
  for await (const job of jobs) {
    if (
      job.ref === "main" &&
      job.name === "pages" &&
      job.status === "success"
    ) {
      count += 1;
      console.log(`pages #${count} ran for ${job.duration} seconds`);
      sum += job.duration;
      if (count === 5) {
        break;
      }
    }
  }

  const avgRuntime = Math.round(sum / count) + 30;

  const NEXT_BUILD = moment(schedules.map((x) => x.next_run_at).sort()[0]);

  console.log(
    `Next pipeline will run on ${NEXT_BUILD}, likely for ${avgRuntime} seconds`
  );

  return NEXT_BUILD.add(avgRuntime, "seconds");
};

const main = async () => {
  console.log("Loading data and calculate averages");

  const stats = require("./people.json");
  const averages = getAverages(stats);

  const nextBuild = await getNextBuildFinished();

  fs.writeFileSync(
    path.join(__dirname, "stats.json"),
    JSON.stringify({
      lastBuild: moment().toISOString(),
      nextBuild: nextBuild.toISOString(),
      commit: process.env.CI_COMMIT_SHORT_SHA || "00000000",
      people: stats.concat(averages),
    })
  );
};

main()
  .then(() => {
    console.log("Successfully parsed data");
  })
  .catch((e) => {
    console.log("Unhandled error");
    console.log(e);
    process.exit(1);
  });
