#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
const moment = require("moment");

const asyncTasks = require("./lib/concurrency");
const { GitLabAPIIterator, gitlabAPI, compileEmoji } = require("./lib/api");
const { getDB } = require("./lib/db_utils");
const {
  matchRegex,
  parseAssignment,
  getLastUpdateKey,
} = require("./lib/utils");

const USER_WITHOUT_EVENTS = "USER_WITHOUT_EVENTS";

let lastUpdate = {};

const getEvents = async () => {
  let events = [];

  const mrs = await getDB();

  for (const [, mr] of Object.entries(mrs)) {
    const key = getLastUpdateKey(mr.references.full);

    if (!lastUpdate[key] || mr.updatedAt > lastUpdate[key]) {
      lastUpdate[key] = mr.updatedAt;
    }

    if (
      mr &&
      mr.author &&
      mr.author.username === "gitlab-bot" &&
      mr.webUrl &&
      mr.webUrl.includes("security-products/tests/")
    ) {
      continue;
    }

    const author = `@${mr.author.username}`;

    const mapAssignee = (assignee, { createdAt, id }, type) => ({
      type,
      createdAt: moment(createdAt),
      id,
      reference: mr.references.full,
      assignee,
      author,
      self: author === assignee,
    });

    const mrAssignmentEvents = (mr.notes || []).flatMap(
      ({ body = "", ...note }) => {
        if (matchRegex.test(body)) {
          return parseAssignment(body).map((a) =>
            mapAssignee(a, note, "reassigned")
          );
        }

        return [];
      }
    );

    events.push(...mrAssignmentEvents);

    /**
     * The MR hasn't been reassigned yet, thus we are creating an assignment for all current assignees
     */
    if (!mrAssignmentEvents.length) {
      const mrInitialAssignment = (mr.assignees || []).map((a) =>
        mapAssignee(`@${a.username}`, mr, "initial")
      );

      events.push(...mrInitialAssignment);
    }
  }

  return events;
};

const defineMapEventEntries = (now, days, sum) => ([username, events]) => {
  const counts = {};

  for (const event of events) {
    if (event.self) {
      continue;
    }
    const day = now.diff(event.createdAt, "days");

    counts[day] = (counts[day] || 0) + 1;
  }

  const assignEvents = days.map(([day, offset]) => ({
    date: day,
    value: counts[offset] || 0,
  }));

  const count7 = sum(assignEvents.slice(-7));
  const count30 = sum(assignEvents);

  return [
    username,
    {
      assignEvents,
      count7,
      count30,
      avg7: parseFloat((count7 / 7).toFixed(1)),
      avg30: parseFloat((count30 / 30).toFixed(1)),
    },
  ];
};

const getOverview = async () => {
  const events = await getEvents();

  const reduced = events
    .sort((a, b) => {
      if (b.createdAt.isBefore(a.createdAt)) return 1;
      if (a.createdAt.isBefore(b.createdAt)) return -1;
      return 0;
    })
    .reduce((r, v, i, a, k = v.assignee) => {
      (r[k] || (r[k] = [])).push(v);
      return r;
    }, {});

  const days = [];

  const now = moment();

  let loop = moment();
  const end = moment().subtract(1, "month");

  while (loop.isAfter(end, "day")) {
    days.unshift([loop.format("YYYY-MM-DD"), now.diff(loop, "days")]);
    loop.subtract(1, "day");
  }

  console.log(days);

  const sum = (dataPoints) =>
    dataPoints.reduce((agg, { value }) => agg + value, 0);

  const mapEventEntries = defineMapEventEntries(now, days, sum);

  const overview = Object.entries(reduced).map(mapEventEntries);

  overview.push(mapEventEntries([USER_WITHOUT_EVENTS, []]));

  return Object.fromEntries(overview);
};

const addSizeToAvatarUrl = (avatarUrl, size = "32") => {
  const url = new URL(avatarUrl);
  const params = new URLSearchParams(url.search);
  if (url.hostname.includes("gravatar.com")) {
    params.set("s", size);
  } else {
    params.set("width", size);
  }
  url.search = params.toString();
  return url.toString();
};

const mapTypes = (types) => {
  return (Array.isArray(types) ? types : [types]).map((t) =>
    t.trim().replace(/\s+/, "::").replace(/_/g, " ")
  );
};

const mapPerson = async (overview, person) => {
  const {
    username: rouletteUsername,
    available: rouletteAvailable,
    projects,
    tz_offset_hours,
  } = person;

  const { data: users } = await gitlabAPI.get("/users", {
    params: { username: rouletteUsername },
  });

  const gitLabUser = users.find(
    (x) =>
      x.username.toLocaleLowerCase() === rouletteUsername.toLocaleLowerCase()
  );

  if (!gitLabUser) {
    throw new Error(`No exact match found for: ${rouletteUsername}`);
  }

  const { id, username, avatar_url, name, web_url: webUrl } = gitLabUser;

  const { data: status } = await gitlabAPI.get(`/users/${id}/status`);

  const allMRs = new GitLabAPIIterator("/merge_requests", {
    assignee_id: id,
    state: "opened",
    scope: "all",
  });

  const mrs = [];

  for await (const mr of allMRs) {
    const { author, references, web_url: webUrl } = mr;

    if (author.username === username) {
      continue;
    }

    if (
      !references.full.startsWith("gitlab-org/") &&
      !references.full.startsWith("gitlab-com/www-gitlab-com")
    ) {
      continue;
    }

    mrs.push({
      webUrl,
      reference: references.full,
    });
  }

  let { emoji, message_html: messageHtml } = status;

  const available =
    rouletteAvailable &&
    !(
      ["red_circle", "palm_tree"].includes(emoji) ||
      /pto|ooo|parental\s*leave/i.test(messageHtml)
    );

  if (emoji) {
    emoji = await compileEmoji(emoji);
  }

  if (messageHtml) {
    messageHtml = messageHtml.replace(/<gl-emoji.+?>(.+?)<\/gl-emoji>/g, "$1");
  }

  console.log(
    `Retrieved data for ${name}: ${mrs.length} MRs – ${emoji} ${messageHtml} `
  );

  return {
    key: username,
    user: {
      username: `@${username}`,
      name,
      avatarUrl: addSizeToAvatarUrl(avatar_url),
      webUrl,
      type: mapTypes(projects.gitlab),
      tzOffset: tz_offset_hours * 60,
    },
    status: {
      available,
      emoji,
      messageHtml,
    },
    stats: overview[`@${username}`] || overview[USER_WITHOUT_EVENTS],
    mrs: mrs.sort((a, b) => {
      return a.reference < b.reference ? -1 : 1;
    }),
  };
};

const mapPersonRetry = async (overview, person, retry = 3) => {
  try {
    return await mapPerson(overview, person);
  } catch (e) {
    if (retry > 0) {
      return await mapPersonRetry(overview, person, retry - 1);
    }
    return {
      type: "error",
      message: e.message,
      stack: e.stack,
    };
  }
};

const main = async () => {
  const allThePeople = require("./roulette.json");

  const GitLabProjectFolks = allThePeople.filter((x) => {
    return x.projects && x.projects.gitlab;
  });

  const overview = await getOverview();

  fs.writeFileSync(
    path.join(__dirname, "mr_db", "last_update"),
    JSON.stringify(lastUpdate)
  );

  const stats = [];

  await Promise.all(
    asyncTasks(
      GitLabProjectFolks,
      async (person) => {
        stats.push(await mapPersonRetry(overview, person));
      },
      5
    )
  );

  fs.writeFileSync(path.join(__dirname, "people.json"), JSON.stringify(stats));
};

main()
  .then(() => {
    console.log("Successfully parsed data");
  })
  .catch((e) => {
    console.log("Unhandled error");
    console.log(e);
    process.exit(1);
  });
